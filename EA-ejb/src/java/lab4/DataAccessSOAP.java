/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4;

import java.util.Date;
import javax.ejb.LocalBean;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;

/**
 *
 * @author student
 */
@WebService(serviceName = "DataAccessSOAP")
@Stateless()
@LocalBean
public class DataAccessSOAP {
    
    private String OstatnieDane;
    private Date OstatniDostep;
    
    public String getOstatnieDane(){
        return OstatniDostep + " | " + OstatnieDane;
    }

    public void setOstatnieDane(String dane, Date data){
        this.OstatnieDane=dane;
        this.OstatniDostep=data;
    }
   
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "DostepKlienta")
    public String DostepKlienta(@WebParam(name = "dane") String dane) {
        String odpowiedz = "Ostatnie dane: " + this.getOstatnieDane();
        this.setOstatnieDane(dane, new java.util.Date());
        return odpowiedz;
    }
}
